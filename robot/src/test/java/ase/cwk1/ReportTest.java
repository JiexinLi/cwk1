// COMP5911M Coursework 1, Task 1

package ase.cwk1;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;


class ReportTest {
  @Test
  void printReport() throws IOException {
    ArrayList<Machine> line = new ArrayList<>();
    line.add(new Machine("mixer", "left"));

    Machine extruder = new Machine("extruder", "centre");
    extruder.addItem("paste");
    line.add(extruder);

    Machine oven = new Machine("oven", "right");
    oven.addItem("tubes");
    line.add(oven);

    Robot robot = new Robot();
    robot.moveTo(extruder);
    robot.pick();

    StringWriter out = new StringWriter();
    Report report = new Report(out);
    report.print(line, robot);

    String expectedText =
      "FACTORY REPORT\n\n" +
      "Machine mixer\nMachine extruder\n" +
      "Machine oven item=tubes\n\n" +
      "Robot location=extruder item=paste\n\n" +
      "END\n";

    assertThat(out.toString(), is(expectedText));
  }
}
