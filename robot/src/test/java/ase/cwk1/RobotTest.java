// COMP5911M Coursework 1, Task 1

package ase.cwk1;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class RobotTest {
  private Machine slicer;
  private Machine oven;
  private Robot robot;

  @BeforeEach
  void setUp() {
    slicer = new Machine("slicer", "left");
    oven = new Machine("oven", "middle");
    robot = new Robot();
    slicer.addItem("chips");
  }

  @Test
  void initialConfiguration() {
    assertAll(
      () -> assertThat(slicer.getItem(), is("chips")),
      () -> assertNull(oven.getItem()),
      () -> assertNull(robot.getLocation()),
      () -> assertNull(robot.getItem())
    );
  }

  @Test
  void moveItem() {
    robot.moveTo(slicer);
    robot.pick();
    robot.moveTo(oven);
    robot.release();

    assertAll(
      () -> assertThat(robot.getLocation(), is(oven)),
      () -> assertNull(robot.getItem()),
      () -> assertThat(oven.getItem(), is("chips")),
      () -> assertNull(slicer.getItem())
    );
  }
}
