// COMP5911M Coursework 1, Task 1

package ase.cwk1;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

import java.io.PrintWriter;
import java.util.ArrayList;


public class Report {
  private Writer output;

  public Report(Writer output) {
    this.output = output;
  }

  public void print(List<Machine> machines, Robot robot) throws IOException {
    output.write("FACTORY REPORT\n\n");

    for (Machine machine: machines) {
      machine.printMachine();
      if (machine.getItem() != null) {
        machine.printItem();
      }
      output.write("\n");
    }
    output.write("\n");

    output.write("Robot");
    if (robot.getLocation() != null) {
      robot.printRobot();
    }
    if (robot.getItem() != null) {
      robot.printItem();
    }
    output.write("\n");

    output.write("\nEND\n");
  }
}
