// COMP5911M Coursework 1, Task 1

package ase.cwk1;

import java.io.Writer;

public class Machine {
  private String name;
  private String location;
  private String item;
  private Writer output;

  public Machine(String name, String location) {
    this.name = name;
    this.location = location;
  }

  public String getName() {
    return name;
  }

  public String getLocation() {
    return location;
  }

  public String getItem() {
    return item;
  }

  public void addItem(String item) {
    this.item = item;
  }

  public String removeItem() {
    String removed = item;
    item = null;
    return removed;
  }

  public void printMachine() {
    output.write("Machine " + name);
  }

  public void printItem() {
    output.write(" item=" + item);
  }
}
