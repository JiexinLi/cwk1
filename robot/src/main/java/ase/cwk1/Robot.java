// COMP5911M Coursework 1, Task 1

package ase.cwk1;

import java.io.Writer;

public class Robot {
  private Machine location;
  private String item;
  private Writer output;

  public Machine getLocation() {
    return location;
  }

  public void moveTo(Machine machine) {
    location = machine;
  }

  public String getItem() {
    return item;
  }

  public void pick() {
    item = location.removeItem();
  }

  public void release() {
    location.addItem(item);
    item = null;
  }

  public void printRobot() {
    output.write(" location=" + location.getName());
  }

  public void printItem() {
    output.write(" item=" + item);
  }
}
