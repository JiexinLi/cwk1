# COMP5911M Coursework 1, Task 2

Run the unit tests on Linux or a Mac with

    ./gradlew test

On Windows, just omit the `./` from the start of the command.

**NOTE: This may be very slow the first time that it runs, as it may
need to download Gradle and unit testing libraries.**

All the tests should pass.

To clean up after running tests, do

    ./gradlew clean
